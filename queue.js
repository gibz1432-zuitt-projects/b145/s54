let collection = [];

// Write the queue functions below.

// 1. Output all the elements of the queue
const print = () => {
    return collection;
}




// 2. Adds element to the rear of the queue
const enqueue = (element) => {
    return collection = [...collection,element]
}

// addRearElement('John',collection)

// const enqueue = (element) => {
//     return collection = [...collection,element]
// }


// 3. Removes element from the front of the queue

const dequeue = () => {
    let element;
    [element, ...collection] = collection;
    return collection;
}
// 4. Show element at the front

const front = () => {
    return collection[0];
}

// 5. Show the total number of elements

const size = () => {
   return collection.length
}



// 6. Outputs a Boolean value describing whether queue is empty or not

const isEmpty = () => {
    if(collection.length > 0 ) return false
    return true
}


// 7. Export your functions
module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};